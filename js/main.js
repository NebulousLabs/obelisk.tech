var formatNumber = function(n, minFrac) {
  var options = {}
  if (minFrac) {
    options.minimumFractionDigits = minFrac
  }
  return n.toLocaleString(n, options)
}

var formatDollars = function(d, minFrac) {
  if (!minFrac) {
    minFrac = 2
  }
  if (d < 0) {
    return '-$' + formatNumber(-d, minFrac)
  } else {
    return '$' + formatNumber(d, minFrac)
  }
}

// NOTE: Month is ZERO BASED here!
var saleEndTime = Date.UTC(2018, 11, 8, 22, 0, 0)
var isSaleOver = false

var MS_PER_SEC = 1000
var MS_PER_MIN = MS_PER_SEC * 60
var MS_PER_HOUR = MS_PER_MIN * 60
var MS_PER_DAY = MS_PER_HOUR * 24

// Countdown timer
var updatePresaleTimer = function() {
  var currTime = new Date()
  var timeRemaining = saleEndTime - currTime.getTime()
  if (timeRemaining <= 0) {
    $('#countdown-timer').text('SALE OVER')
    $('#countdown-container').css('width', '230px')
    $('.hide-when-sale-closed').css('visibility', 'hidden')
    $('.disp-when-sale-closed').css('display', 'block')
    $('.no-disp-when-sale-closed').css('display', 'none')
    clearInterval(interval)
    return
  }

  var days = Math.floor(timeRemaining / MS_PER_DAY)
  timeRemaining -= days * MS_PER_DAY

  var hours = Math.floor(timeRemaining / MS_PER_HOUR)
  timeRemaining -= hours * MS_PER_HOUR

  var mins = Math.floor(timeRemaining / MS_PER_MIN)
  timeRemaining -= mins * MS_PER_MIN

  var secs = Math.floor(timeRemaining / MS_PER_SEC)

  daysStr = days < 10 ? '0' + days : days
  hoursStr = hours < 10 ? '0' + hours : hours
  minsStr = mins < 10 ? '0' + mins : mins
  secsStr = secs < 10 ? '0' + secs : secs

  $('.countdown-dd').text(daysStr)
  $('.countdown-hh').text(hoursStr)
  $('.countdown-mm').text(minsStr)
  $('.countdown-ss').text(secsStr)
}

var interval = setInterval(updatePresaleTimer, 1000)

updatePresaleTimer()

function updateOrderCounts() {
  fetch('https://portal.obelisk.tech/api/orderCounts')
    .then(function(resp) {
      return resp.json()
    })
    .then(function(result) {
      var counts = result.counts
      var available = result.available
      var percent
      if (counts && available) {
        SC1Gen1Count = counts['SC1-HB-G1']
        SC1Gen1Avail = available['SC1-HB-G1']
        percent = (SC1Gen1Count / SC1Gen1Avail) * 100
        if (SC1Gen1Count) {
          $('#sc1gen1-sold').text(formatNumber(SC1Gen1Count))
          $('#sc1gen1-avail').text(formatNumber(SC1Gen1Avail))
          $('#sc1gen1-percent').css('width', '' + percent + '%')
        }

        SC1Gen2Count = counts['SC1-HB-G2']
        SC1Gen2Avail = available['SC1-HB-G2']
        SC1ConvCount = counts['SC1-G2-CONV']
        totalCount = SC1Gen2Count + SC1ConvCount
        percent = (totalCount / SC1Gen2Avail) * 100
        if (totalCount) {
          $('#sc1gen2-sold').text(formatNumber(totalCount))
          $('#sc1gen2-avail').text(formatNumber(SC1Gen2Avail))
          $('#sc1gen2-percent').css('width', '' + percent + '%')
        }
      }
    })
}

function showProducts() {
  document.getElementById('products-dropdown').classList.toggle('show')
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropdown-btn')) {
    var dropdowns = document.getElementsByClassName('dropdown-content')
    var i
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i]
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show')
      }
    }
  }
}

// updateOrderCounts()

function setTab(evt, tabName) {
  // Declare all variables
  var i, tabcontent, tablinks

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName('tab-content')
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = 'none'
  }

  // Get all elements with class="tab-link" and remove the class "active"
  tablinks = document.getElementsByClassName('tab-link')
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(' active', '')
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(tabName + '-content').style.display = 'block'
  evt.currentTarget.className += ' active'

  // Add a hash to the URL
  window.location.hash = tabName
}
